package com.example.boilerplatetests.ui.details

import android.annotation.SuppressLint
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.example.boilerplatetests.models.RepoModel
import com.example.boilerplatetests.ui.components.RepoHeaderComponent
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.TimeZone

@Composable
fun DetailScreen(repoModel: RepoModel?) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp)
    ) {
        if (repoModel == null) {
            Text(text = "Invalid Repo")
        } else {
            RepoHeaderComponent(repoModel, withpadding = false)
            Spacer(modifier = Modifier.size(10.dp))
            Text(text = "Last Updated: ${parseDateTime(repoModel.updatedAt)}")
            val context = LocalContext.current
            Spacer(modifier = Modifier.height(10.dp))
            Text(text = "Web Content:")
            AndroidView(factory = {
                WebView(context).apply {
                    webViewClient = WebViewClient()
                    repoModel.htmlUrl?.let {
                        loadUrl(repoModel.htmlUrl)
                    }
                }
            })
        }
    }
}

@SuppressLint("SimpleDateFormat")
fun parseDateTime(date: String?): String {
    if (date == null) return "Error"
    val tz: TimeZone = TimeZone.getTimeZone("UTC")
    val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    df.timeZone = tz
    val newDate = df.parse(date)
    return SimpleDateFormat("MM-dd-yy HH:ss").format(newDate)
}