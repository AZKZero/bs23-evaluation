package com.example.boilerplatetests.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.boilerplatetests.models.RepoModel
import com.example.boilerplatetests.ui.theme.Purple40

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun RepoHeaderComponent(repo: RepoModel?, withpadding: Boolean = true) {
    Column(modifier = Modifier.padding(if (withpadding) 10.dp else 0.dp)) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            GlideImage(
                model = repo?.owner?.avatarUrl, contentDescription = "avatar",
                Modifier
                    .height(20.dp)
                    .width(20.dp)
            )
            Spacer(modifier = Modifier.width(5.dp))
            Text(text = repo?.fullName ?: "")
            Spacer(modifier = Modifier.width(5.dp))
            Button(
                contentPadding = PaddingValues(vertical = 5.dp, horizontal = 10.dp),
                onClick = {},
                colors = ButtonDefaults.elevatedButtonColors(containerColor = Purple40.copy(alpha = 0.1f), contentColor = Color.Black)
            ) {
                Text(text = repo?.visibility ?: "", fontSize = TextUnit(10f, TextUnitType.Sp))
            }
            Spacer(modifier = Modifier.weight(1f))
            Button(
                contentPadding = PaddingValues(vertical = 10.dp, horizontal = 15.dp),
                onClick = {},
                colors = ButtonDefaults.elevatedButtonColors(containerColor = Purple40.copy(alpha = 0.6f), contentColor = Color.White)
            ) {
                Icon(Icons.Filled.Star, "star")
                Spacer(modifier = Modifier.width(2.dp))
                Text(text = repo?.stargazersCount.toString())
            }
        }
        Spacer(modifier = Modifier.height(5.dp))
        Text(text = repo?.description ?: "")
        LazyRow(modifier = Modifier.fillMaxWidth()) {
            items(repo?.topics ?: emptyList()) {

                Button(onClick = {}, modifier = Modifier.padding(5.dp)) {
                    Text(text = it ?: "")
                }
            }
        }
    }
}