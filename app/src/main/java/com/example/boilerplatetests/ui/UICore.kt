package com.example.boilerplatetests.ui

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.ScaffoldDefaults
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.boilerplatetests.ui.details.DetailScreen
import com.example.boilerplatetests.ui.list.ListScreen
import com.example.boilerplatetests.ui.list.ListViewModel
import com.example.boilerplatetests.utils.Routes

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun UICore(navController: NavHostController = rememberNavController(), viewModel: ListViewModel = androidx.lifecycle.viewmodel.compose.viewModel()) {
    ScaffoldDefaults.contentWindowInsets  // A surface container using the 'background' color from the theme
    val selectedRepo by viewModel.selectedRepoLiveData.observeAsState()
    var title by remember {
        mutableStateOf("Home")
    }
    Scaffold(
        topBar = {
            TopAppBar(title = { Text(text = title) },
                navigationIcon = {
                    if (navController.previousBackStackEntry != null) {
                        IconButton(onClick = { navController.navigateUp() }) {
                            Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = "back"
                            )
                        }
                    }
                })
        }
    ) {
        NavHost(navController = navController, startDestination = Routes.LIST.name, modifier = Modifier.padding(paddingValues = it)) {
            composable(route = Routes.LIST.name) {
                title = "Home"
                ListScreen() {
                    viewModel.selectedRepoLiveData.postValue(it)
                    navController.navigate(Routes.DETAILS.name)
                }
            }
            composable(route = Routes.DETAILS.name) {
                title = selectedRepo?.name ?: ""
                DetailScreen(selectedRepo)
            }

        }
    }

}