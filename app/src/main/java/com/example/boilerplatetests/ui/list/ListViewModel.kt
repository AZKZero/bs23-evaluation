package com.example.boilerplatetests.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.boilerplatetests.models.GitHubResponseModel
import com.example.boilerplatetests.models.RepoModel
import com.example.boilerplatetests.models.ResponseState
import com.example.boilerplatetests.models.ResponseWrapper
import com.example.boilerplatetests.utils.ApiService
import com.example.boilerplatetests.utils.RetrofitUtil
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class ListViewModel : ViewModel() {
    //    private val service: ApiService by lazy {  }
    private val repository by lazy { ListRepository(RetrofitUtil.getRetrofitService(ApiService::class.java)) }
    fun getRepos(): Flow<PagingData<RepoModel>> = repository.getRepos().cachedIn(viewModelScope)

    val repoLiveData by lazy { MutableLiveData<ResponseWrapper<GitHubResponseModel>>() }

    val selectedRepoLiveData by lazy { MutableLiveData<RepoModel>() }
    /* fun getRepo() {
         viewModelScope.launch {
             try {
                 val response = service.getRepos()
                 if (response.isSuccessful) {
                     repoLiveData.postValue(ResponseWrapper(state = ResponseState.SUCCESS, data = response.body()))
                 }
             } catch (e: Exception) {

             }
         }
     }*/
}