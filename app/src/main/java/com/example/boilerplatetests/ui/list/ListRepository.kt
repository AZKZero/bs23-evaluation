package com.example.boilerplatetests.ui.list

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.example.boilerplatetests.utils.ApiService
import com.example.boilerplatetests.utils.RetrofitUtil
import com.example.boilerplatetests.utils.paging.RepoPagingSource

class ListRepository(private val service: ApiService) {

    fun getRepos() = Pager(
        config = PagingConfig(
            pageSize = 10,
        ),
        pagingSourceFactory = {
            RepoPagingSource(service)
        }
    ).flow
}