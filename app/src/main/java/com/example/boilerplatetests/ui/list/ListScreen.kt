package com.example.boilerplatetests.ui.list

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemKey
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.example.boilerplatetests.models.RepoModel
import com.example.boilerplatetests.ui.components.RepoHeaderComponent

@OptIn(ExperimentalMaterial3Api::class, ExperimentalGlideComposeApi::class)
@Composable
fun ListScreen(viewModel: ListViewModel = androidx.lifecycle.viewmodel.compose.viewModel(), onClick: (RepoModel?) -> Unit) {
    /*LaunchedEffect(key1 = Unit) {
        viewModel.getRepo()
    }*/
    val repos = viewModel.getRepos().collectAsLazyPagingItems()
    Column(modifier = Modifier.fillMaxSize()) {
        LazyColumn(modifier = Modifier.fillMaxSize()) {
            items(count = repos.itemCount, key = repos.itemKey { it.url ?: "" }) {
                Card(modifier = Modifier.padding(vertical = 10.dp), onClick = {
                    onClick(repos[it])
                }) {
                    RepoHeaderComponent(repos[it])
                }
            }
        }
        /*val listResponse by viewModel.repoLiveData.observeAsState()
        when (listResponse?.state) {

            ResponseState.LOADING -> CircularProgressIndicator()
            ResponseState.SUCCESS -> {
                listResponse?.data?.items?.let { list ->
                    LazyColumn(modifier = Modifier.fillMaxSize()) {
                        items(list) { repo ->
                            Card(modifier = Modifier.padding(vertical = 10.dp), onClick = {
                                onClick(repo)
                            }) {
                                RepoHeaderComponent(repo)
                            }
                        }
                    }
                }
            }

            ResponseState.FAILURE -> {}
            ResponseState.NETWORK_FAILURE -> {}
            null -> {}
        }*/
    }
}