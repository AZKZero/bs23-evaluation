package com.example.boilerplatetests

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.lifecycle.lifecycleScope
import com.example.boilerplatetests.ui.UICore
import com.example.boilerplatetests.ui.theme.BoilerPlateTestsTheme
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            delay(300)
//            window.setBackgroundDrawableResource(android.R.color.transparent)
            setContent {
                BoilerPlateTestsTheme {
                    UICore()
                }
            }
        }

    }
}

