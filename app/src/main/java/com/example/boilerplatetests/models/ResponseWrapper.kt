package com.example.boilerplatetests.models

import okhttp3.ResponseBody

enum class ResponseState {
    LOADING,
    SUCCESS,
    FAILURE,
    NETWORK_FAILURE
}

data class ResponseWrapper<T>(
    val state: ResponseState,
    val data: T?,
    val error: ResponseBody? = null
)
