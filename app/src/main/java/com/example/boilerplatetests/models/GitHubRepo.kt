package com.example.boilerplatetests.models

import com.google.gson.annotations.Expose

import androidx.annotation.Keep

import com.google.gson.annotations.SerializedName

@Keep
data class GitHubResponseModel(
    @SerializedName("incomplete_results")
    @Expose
    val incompleteResults: Boolean? = null,
    @SerializedName("total_count")
    @Expose
    val totalCount: Int? = null,
    @SerializedName("items")
    @Expose
    val items: List<RepoModel>? = null,
)

@Keep
data class RepoModel(
    @SerializedName("allow_forking")
    @Expose
    val allowForking: Boolean? = null,
    @SerializedName("archive_url")
    @Expose
    val archiveUrl: String? = null,
    @SerializedName("archived")
    @Expose
    val archived: Boolean? = null,
    @SerializedName("assignees_url")
    @Expose
    val assigneesUrl: String? = null,
    @SerializedName("blobs_url")
    @Expose
    val blobsUrl: String? = null,
    @SerializedName("branches_url")
    @Expose
    val branchesUrl: String? = null,
    @SerializedName("clone_url")
    @Expose
    val cloneUrl: String? = null,
    @SerializedName("collaborators_url")
    @Expose
    val collaboratorsUrl: String? = null,
    @SerializedName("comments_url")
    @Expose
    val commentsUrl: String? = null,
    @SerializedName("commits_url")
    @Expose
    val commitsUrl: String? = null,
    @SerializedName("compare_url")
    @Expose
    val compareUrl: String? = null,
    @SerializedName("contents_url")
    @Expose
    val contentsUrl: String? = null,
    @SerializedName("contributors_url")
    @Expose
    val contributorsUrl: String? = null,
    @SerializedName("created_at")
    @Expose
    val createdAt: String? = null,
    @SerializedName("default_branch")
    @Expose
    val defaultBranch: String? = null,
    @SerializedName("deployments_url")
    @Expose
    val deploymentsUrl: String? = null,
    @SerializedName("description")
    @Expose
    val description: String? = null,
    @SerializedName("disabled")
    @Expose
    val disabled: Boolean? = null,
    @SerializedName("downloads_url")
    @Expose
    val downloadsUrl: String? = null,
    @SerializedName("events_url")
    @Expose
    val eventsUrl: String? = null,
    @SerializedName("fork")
    @Expose
    val fork: Boolean? = null,
    @SerializedName("forks")
    @Expose
    val forks: Int? = null,
    @SerializedName("forks_count")
    @Expose
    val forksCount: Int? = null,
    @SerializedName("full_name")
    @Expose
    val fullName: String? = null,
    @SerializedName("html_url")
    @Expose
    val htmlUrl: String? = null,
    @SerializedName("id")
    @Expose
    val id: Int? = null,
    @SerializedName("name")
    @Expose
    val name: String? = null,
    @SerializedName("node_id")
    @Expose
    val nodeId: String? = null,
    @SerializedName("notifications_url")
    @Expose
    val notificationsUrl: String? = null,
    @SerializedName("open_issues")
    @Expose
    val openIssues: Int? = null,
    @SerializedName("open_issues_count")
    @Expose
    val openIssuesCount: Int? = null,
    @SerializedName("owner")
    @Expose
    val owner: Owner? = null,
    @SerializedName("private")
    @Expose
    val `private`: Boolean? = null,
    @SerializedName("stargazers_count")
    @Expose
    val stargazersCount: Int? = null,
    @SerializedName("topics")
    @Expose
    val topics: List<String?>? = null,
    @SerializedName("trees_url")
    @Expose
    val treesUrl: String? = null,
    @SerializedName("updated_at")
    @Expose
    val updatedAt: String? = null,
    @SerializedName("url")
    @Expose
    val url: String? = null,
    @SerializedName("visibility")
    @Expose
    val visibility: String? = null,
    @SerializedName("watchers")
    @Expose
    val watchers: Int? = null,
    @SerializedName("watchers_count")
    @Expose
    val watchersCount: Int? = null,
    @SerializedName("web_commit_signoff_required")
    @Expose
    val webCommitSignoffRequired: Boolean? = null
)

@Keep
data class Owner(
    @SerializedName("avatar_url")
    @Expose
    val avatarUrl: String? = null,
    @SerializedName("events_url")
    @Expose
    val eventsUrl: String? = null,
    @SerializedName("followers_url")
    @Expose
    val followersUrl: String? = null,
    @SerializedName("following_url")
    @Expose
    val followingUrl: String? = null,
    @SerializedName("gists_url")
    @Expose
    val gistsUrl: String? = null,
    @SerializedName("gravatar_id")
    @Expose
    val gravatarId: String? = null,
    @SerializedName("html_url")
    @Expose
    val htmlUrl: String? = null,
    @SerializedName("id")
    @Expose
    val id: Int? = null,
    @SerializedName("login")
    @Expose
    val login: String? = null,
    @SerializedName("node_id")
    @Expose
    val nodeId: String? = null,
    @SerializedName("organizations_url")
    @Expose
    val organizationsUrl: String? = null,
    @SerializedName("received_events_url")
    @Expose
    val receivedEventsUrl: String? = null,
    @SerializedName("repos_url")
    @Expose
    val reposUrl: String? = null,
    @SerializedName("site_admin")
    @Expose
    val siteAdmin: Boolean? = null,
    @SerializedName("starred_url")
    @Expose
    val starredUrl: String? = null,
    @SerializedName("subscriptions_url")
    @Expose
    val subscriptionsUrl: String? = null,
    @SerializedName("type")
    @Expose
    val type: String? = null,
    @SerializedName("url")
    @Expose
    val url: String? = null
)