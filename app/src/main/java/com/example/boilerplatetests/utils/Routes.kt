package com.example.boilerplatetests.utils

enum class Routes {
    DETAILS,
    LIST
}