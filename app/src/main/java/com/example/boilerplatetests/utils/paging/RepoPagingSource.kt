package com.example.boilerplatetests.utils.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.boilerplatetests.models.RepoModel
import com.example.boilerplatetests.utils.ApiService

class RepoPagingSource(
    private val service: ApiService,
) : PagingSource<Int, RepoModel>() {
    override fun getRefreshKey(state: PagingState<Int, RepoModel>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, RepoModel> {
        return try {
            val page = params.key ?: 1
            val response = service.getRepos(page = page)
            val repos = response.body()!!.items!!
            LoadResult.Page(
                data = repos,
                prevKey = if (page == 1) null else page.minus(1),
                nextKey = if (repos.isEmpty()) null else page.plus(1),
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}