package com.example.boilerplatetests.utils

import com.example.boilerplatetests.models.GitHubResponseModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/search/repositories")
    suspend fun getRepos(@Query("sort") sortBy: String = "stars", @Query("q") filter: String = "android", @Query("page") page: Int = 1, @Query("per_page") perPage: Int = 10): Response<GitHubResponseModel>
}