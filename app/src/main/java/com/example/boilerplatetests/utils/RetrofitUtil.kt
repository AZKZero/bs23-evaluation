package com.example.boilerplatetests.utils

import com.example.boilerplatetests.Core
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitUtil {
    private fun getRetrofitClient() = Retrofit.Builder()
        .baseUrl("https://api.github.com")
        .client(OkHttpClient
            .Builder()
            .addInterceptor(HttpLoggingInterceptor()
                .apply { level = HttpLoggingInterceptor.Level.BODY }
            )
            .addInterceptor { request ->
                request.proceed(
                    request
                        .request()
                        .newBuilder()
                        .addHeader("package", Core.appContext().packageName)
                        .build()
                )
            }
            .build()
        )
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun <T> getRetrofitService(service: Class<T>): T = getRetrofitClient().create(service)
}