package com.example.boilerplatetests

import android.annotation.SuppressLint
import android.app.Application

class Core : Application() {
    companion object {
        @SuppressLint("StaticFieldLeak")
        private var instance: Core? = null
        fun appContext(): Core {
            return instance as Core
        }
    }

    init {
        instance = this
    }
}